"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.supported = supported;
exports.copy = copy;
exports.copySync = copySync;

var _isInBrowser = require("./isInBrowser");

function supported() {
  return (0, _isInBrowser.isInBrowser)() && window.document.queryCommandSupported('copy');
}

function copy(text) {
  return new Promise(function (resolve, reject) {
    try {
      var succeeded = copySync(text);
      succeeded ? resolve() : reject();
    } catch (e) {
      reject(e);
    }
  });
}

function copySync(text) {
  var isRTL = document.documentElement.getAttribute('dir') == 'rtl';
  var selectNode = document.createElement('span');
  selectNode.innerText = text; // Preserve consecutive spaces and newlines

  selectNode.style.whiteSpace = 'pre';
  selectNode.style.position = 'absolute';
  selectNode.style[isRTL ? 'right' : 'left'] = '-9999px';
  document.body.appendChild(selectNode); // Range

  var range = window.document.createRange();
  range.selectNodeContents(selectNode); // Selection

  var selection = window.getSelection();
  selection.removeAllRanges();
  selection.addRange(range); // Do copy

  try {
    return document.execCommand('copy');
  } finally {
    // Cleanup
    selection.removeAllRanges();
    document.body.removeChild(selectNode);
  }
}