"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "copy", {
  enumerable: true,
  get: function get() {
    return _clipboard.copy;
  }
});
Object.defineProperty(exports, "copySync", {
  enumerable: true,
  get: function get() {
    return _clipboard.copySync;
  }
});
Object.defineProperty(exports, "supported", {
  enumerable: true,
  get: function get() {
    return _clipboard.supported;
  }
});
exports["default"] = void 0;

var _clipboard = require("./lib/clipboard");

var _copy = require("./component/copy");

var _default = _copy.Copy;
exports["default"] = _default;