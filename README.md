# clipboard-react
A lightweight react component for copying text to clipboard, inspired by [clipboard.js](https://clipboardjs.com/).

# Install
```
npm install clipboard-react
```

# Usage
```jsx
import Copy from 'clipboard-react'

<Copy text="hello world" onSuccess={() => alert('Copied')} onError={reason => alert('Failed')}>Copy</Copy>
```
Exec copy action when user click 'Copy'.

# APIs
## `Copy` component
```js
import Copy from 'clipboard-react'

<Copy text="hello world">Copy</Copy>
```

The component props:

|Name|Type|Default|Desc|
|--|--|--|--|
|text|string| |The content will be copied|
|onSuccess| Function| |The callback when copy successfully|
|onError|Function| |The callback when copy failed|
|sniff|bool|true|`true`: do not display when the `document.queryCommandSupported('copy')` command is supported by the browser|
|Otherwise| | |The otherwise props will added to React element|

## `copy` method
Copy imperatively.
```js
import { copy } from 'clipboard-react'

async function doCopy() {
    await copy('hello');
    console.log('Copy successfully');
}
```

## `copySync` method
```js
import { copySync } from 'clipboard-react'

const successed = copySync('hello');
```

## `supported` method
Check whether or not the `document.queryCommandSupported('copy')` command is supported by the browser.
```js
import { supported } from 'clipboard-react'

const isSupported = supported();
```