import React from 'react';
import { copy, supported } from '../lib/clipboard';
import { isInBrowser } from '../lib/isInBrowser'

Copy.defaultProps = {
    sniff: true
}

export function Copy(props) {
    const { text, sniff, onSuccess, onError, ...restProps } = props;

    async function handleCopy() {
        try {
            await copy(text);
            onSuccess && onSuccess();
        } catch(e) {
            onError && onError(e);
        }
    }

    return (
        <If condition={!sniff || !isInBrowser() || supported()}>
            <span {...restProps} onClick={handleCopy}/>
        </If>
    )
}