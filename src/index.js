import { copy, copySync, supported } from './lib/clipboard';
import { Copy } from './component/copy';

export { copy, copySync, supported }

export default Copy;