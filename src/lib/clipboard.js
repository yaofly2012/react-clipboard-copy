
import { isInBrowser } from './isInBrowser'

export function supported() {
    return isInBrowser() && window.document.queryCommandSupported('copy');
}

export function copy(text) {
    return new Promise((resolve, reject) => {
        try {
            const succeeded = copySync(text);
            succeeded ? resolve() : reject();
        } catch(e) {
            reject(e);
        }
    })
}

export function copySync(text) {
    const isRTL = document.documentElement.getAttribute('dir') == 'rtl';
    const selectNode = document.createElement('span');
    selectNode.innerText = text;
    // Preserve consecutive spaces and newlines
    selectNode.style.whiteSpace = 'pre';
    selectNode.style.position = 'absolute';
    selectNode.style[ isRTL ? 'right' : 'left' ] = '-9999px';
    document.body.appendChild(selectNode);

    // Range
    const range = window.document.createRange();
    range.selectNodeContents(selectNode);

    // Selection
    const selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);

    // Do copy
    try {
        return document.execCommand('copy');
    } finally {
        // Cleanup
        selection.removeAllRanges();
        document.body.removeChild(selectNode);
    }
}